package org.idx.application.dob;

import java.util.List;

import lombok.Data;

@Data
public class ConfiguratorDOB {
    private long id;
    
    private String name;
    
    private String displayName;

    private String profileName;

    private String version;
    
    private String className;

    private String type;

    private List<ConfigurationParamsDOB> configParams;


}
