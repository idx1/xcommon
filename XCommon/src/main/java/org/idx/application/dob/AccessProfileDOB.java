package org.idx.application.dob;

import java.util.Date;

import lombok.Data;

@Data
public class AccessProfileDOB {

    private long id;
    
    private String name;

    private String description;
    
    ApplicationProfileDOB applicationProfile;
    
    private EntitlementDOB entitlement;

    private ApplicationRoleDOB appRole;
	
    private boolean valid;
	
    private String createdBy;

    private String updatedBy;

    private Date createdDate;
    
    private Date updatedDate;

}
