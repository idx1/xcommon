package org.idx.application.dob;

import java.util.Date;
import lombok.Data;

@Data
public class EntitlementDOB {

    private long id;
    
    private String name;
    
    private String displayName;

    private String value;

    private boolean valid;

    private String createdBy;

    private String updatedBy;

    private Date createdDate;
    
    private Date updatedDate;
    
    private ApplicationDOB application;

}
