package org.idx.aggregator.dob;

import lombok.Data;

@Data
public class AggregationAttrValueDOB {

	private int id;

	private String value;
}
