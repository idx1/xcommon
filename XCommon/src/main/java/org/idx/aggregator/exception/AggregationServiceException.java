package org.idx.aggregator.exception;

import org.springframework.web.bind.annotation.ResponseStatus;

public class AggregationServiceException extends Exception{

	public AggregationServiceException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public AggregationServiceException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public AggregationServiceException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public AggregationServiceException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public AggregationServiceException() {
		// TODO Auto-generated constructor stub
	}

}
