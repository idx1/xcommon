package org.idx.identity.dob;

import java.io.Serializable;

import lombok.Data;

@Data
public class UserAttributeDOB implements Serializable {

    private int id;
    
    private String name;

    private String displayName;

    private USER_ATTR_TYPE userAttrType;
	
    private String category;

    private int displayLength;
    
    private int columnLength;

}
