package org.idx.prov.accesspolicy.dob;

public enum USER_RULE_APPEND_CONDITION {
	
	AND (1);
	
	private final int userRuleAppendCondition;
	
	USER_RULE_APPEND_CONDITION(int userRuleAppendCondition){
		this.userRuleAppendCondition=userRuleAppendCondition;
	}

	public int getUserRuleAppendCondition(){
		return userRuleAppendCondition;
	}


}
