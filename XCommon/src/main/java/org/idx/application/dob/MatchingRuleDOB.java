package org.idx.application.dob;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

import lombok.Data;

@Data
public class MatchingRuleDOB {

	private int id;
	
	private String idxAttribute;
	
	private String tgtAttribute;
	
	private MATCHING_CONDITION matchingCondition;


}
