package org.idx.application.dob;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

@Data
public class MatchingActionDOB {

	private int id;
	
	private MATCHING_ACTION_CONDITION aggCondition;
	private MATCHING_ACTION aggAction;

}
