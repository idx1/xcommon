package org.idx.application.dob;

 
import lombok.Data;

@Data
public class ApplicationDOB {

    private long id;
    
    private String name;
    
    private String displayName;

    private String description;
    
    private boolean isTrusted;
    
    private long configuratorType;
    
	private ApplicationTypeDOB applicationType = new ApplicationTypeDOB();

    private ConfiguratorDOB configurator = new ConfiguratorDOB();
    
    private AggregationProfileDOB profile = new AggregationProfileDOB();
}
