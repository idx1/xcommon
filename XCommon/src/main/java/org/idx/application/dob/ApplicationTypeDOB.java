package org.idx.application.dob;

import java.util.List;

import lombok.Data;

@Data
public class ApplicationTypeDOB {
    private long id;
    
    private String name;
    
    private String displayName;

    private String description;

    private List<AppTypeParamsDOB> appTypeParams;

}
