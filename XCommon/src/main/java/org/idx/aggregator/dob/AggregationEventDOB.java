package org.idx.aggregator.dob;

import java.util.Date;
import java.util.List;

import org.idx.application.dob.AggregationProfileDOB;
import org.idx.application.dob.EVENT_STATE;

import lombok.Data;

@Data
public class AggregationEventDOB {

	private int id;

	private String uid;
	
	private String uidValue;
	
	private List<AggregationAttributeDOB> attributes;
	
	// NOTE: Isko baad me dekhenge
//	private List<SyncAttribute> childAttributes;
	
	private AggregationResultDOB result;
		
	private String failureReason;
	private EVENT_STATE eventState;
	private Date creationDate;
	private Date actionDate;
	
	private AggregationProfileDOB aggregationProfile;

}
