package org.idx.prov.accesspolicy.dob;

public enum ORG_RULE_APPEND_CONDITION{
	
	AND (1);
	
	private final int orgRuleAppendCondition;
	
	ORG_RULE_APPEND_CONDITION(int orgRuleAppendCondition){
		this.orgRuleAppendCondition=orgRuleAppendCondition;
	}

	public int getOrgRuleAppendCondition(){
		return orgRuleAppendCondition;
	}


}
