package org.idx.scheduler.dob;

import lombok.Data;

@Data
public class ScheduledJobParamsDOB {
	
	private String name;
	private String value;

}
