package org.idx.prov.accesspolicy.dob;

public enum ORG_ATT_RULE_CONDITION {

	EQUALS(1),
	NOT_EQUALS(2),
	CONTAINS(3);
	
	private final int userAttRuleCondition;
	
	ORG_ATT_RULE_CONDITION(int userAttRuleCondition){
		this.userAttRuleCondition=userAttRuleCondition;
	}

	public int getUserAttRuleCondition(){
		return userAttRuleCondition;
	}


}
