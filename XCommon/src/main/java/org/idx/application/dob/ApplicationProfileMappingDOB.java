package org.idx.application.dob;

import java.util.List;

import lombok.Data;

@Data
public class ApplicationProfileMappingDOB {

private String applicationName;
private String appProfileName;
private List<ApplicationProfileDOB> profiles;

}
