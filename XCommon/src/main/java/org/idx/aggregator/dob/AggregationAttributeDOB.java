package org.idx.aggregator.dob;

import java.util.List;

import lombok.Data;

@Data
public class AggregationAttributeDOB {

	private int id;

	
	private String name;
	
	private List<AggregationAttrValueDOB> value;

	private ATTR_TYPE type;
	

}
