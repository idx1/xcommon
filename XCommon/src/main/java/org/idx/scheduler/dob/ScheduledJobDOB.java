package org.idx.scheduler.dob;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import lombok.Data;

@Data
public class ScheduledJobDOB implements Serializable{
	
	private String name;
	private String className;
	private String description;
	private int jobType;
	private Date startDate;
	private Date endDate;
	private Date lastRunTime;
	private Date nextRunTime;
	private String jobStatus;
	private int repeatInterval;
	private JOB_REPEATINTERVAL_TYPE intervalType = JOB_REPEATINTERVAL_TYPE.MINUTES;
	private String cronExpression;
	
	private List<ScheduledJobParamsDOB> jobParams = new ArrayList<ScheduledJobParamsDOB>();


}
