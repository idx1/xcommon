package org.idx.application.dob;

import java.io.Serializable;

import org.springframework.stereotype.Component;

import lombok.Data;

@Data
public class ProfileAttributeDOB implements Serializable{

	private int id;
	
	private String name;
	
	private String targetAttrName;
	
	private PR_ATTR_TYPE type;
	
	private int length;
	
	boolean isKey;

	public enum PR_ATTR_TYPE{
		STRING,NUMBER,BOOLEAN,DATE;
	}

}
