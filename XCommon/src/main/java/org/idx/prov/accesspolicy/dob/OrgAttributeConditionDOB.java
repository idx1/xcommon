package org.idx.prov.accesspolicy.dob;

import lombok.Data;

@Data
public class OrgAttributeConditionDOB {

	private String orgAttribute;
	private String orgAttributeValue;
	private ORG_RULE_APPEND_CONDITION uaCondition;
	private ORG_ATT_RULE_CONDITION ruleCondition;
}
