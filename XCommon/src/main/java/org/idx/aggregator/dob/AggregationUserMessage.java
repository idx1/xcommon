package org.idx.aggregator.dob;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.idx.application.dob.MatchingActionDOB;
import org.idx.application.dob.MatchingRuleDOB;

import lombok.Data;

@Data
public class AggregationUserMessage implements Serializable{
	
	private MatchingRuleDOB matchingRule;
	private List<MatchingActionDOB> matchingActions;
	private Map<String, String> userAttributeMap;
}
