package org.idx.application.dob;

import lombok.Data;

@Data
public class ConfigurationParamsDOB {

	private int id;

	private String name;

	private String description;

	private CONFIG_PARAM_TYPE type;

	private String value;	

	public enum CONFIG_PARAM_TYPE{
		STRING,NUMBER,BOOLEAN,DATE,SECRET;
	}

}
