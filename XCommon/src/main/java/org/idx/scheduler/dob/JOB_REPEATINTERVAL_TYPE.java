package org.idx.scheduler.dob;

public enum JOB_REPEATINTERVAL_TYPE {
	MINUTES (1),
	HOURS (2),
	DAYS (3);
	
	private final int jobRepeatIntervalType;
	
	JOB_REPEATINTERVAL_TYPE(int jobRepeatIntervalType){
		this.jobRepeatIntervalType=jobRepeatIntervalType;
	}

	public int getJobRepeatIntervalType(){
		return jobRepeatIntervalType;
	}


}
