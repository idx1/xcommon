package org.idx.application.dob;

import org.idx.application.dob.ConfigurationParamsDOB.CONFIG_PARAM_TYPE;

import lombok.Data;

@Data
public class AppTypeParamsDOB {

	private int id;

	private String name;

	private String description;

	private APP_TYPE_PARAM_TYPE type;

	private String label;	

	private String value;	

	private boolean editable;	

	public enum APP_TYPE_PARAM_TYPE{
		STRING,NUMBER,BOOLEAN,DATE,SECRET;
	}
}
