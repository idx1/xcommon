package org.idx.application.dob;

import java.util.Date;
import java.util.List;

import lombok.Data;

@Data
public class ApplicationProfileDOB {

    private long id;
    
    private String name;

    private String description;
    
    private String displayName;
    
    private ApplicationDOB application;

    private List<EntitlementDOB> entitlements;

    private List<ApplicationRoleDOB> appRoles;

    private boolean valid;
	
    private String createdBy;

    private String updatedBy;

    private Date createdDate;
    
    private Date updatedDate;

}
