package org.idx.aggregator.dob;

import lombok.Data;

@Data
public class AggregationResultDOB {

	private int id;

	private String entityType;
	
	private String entityIdentifier;
	
	private String aggregationResult;

}
