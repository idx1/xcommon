package org.idx.prov.accesspolicy.dob;

import lombok.Data;

@Data
public class UserAttributeConditionDOB {

	private String userAttribute;
	private String userAttributeValue;
	private USER_RULE_APPEND_CONDITION uaCondition;
	private USER_ATT_RULE_CONDITION ruleCondition;
	

}
