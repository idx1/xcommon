package org.idx.common.rest;

import lombok.Data;

@Data
public class ServerResponse {
	private int statusCode;
	private Object data;
}
