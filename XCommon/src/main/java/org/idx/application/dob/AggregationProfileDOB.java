package org.idx.application.dob;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class AggregationProfileDOB {

	private int id;
	
	private String name;

	private String tableName;
	
	private AGGREGATION_TYPE aggregationType;
	
	private List<ProfileAttributeDOB> profileAttr = new ArrayList<ProfileAttributeDOB>();
	
	private MatchingRuleDOB matchingRule;
	
	private List<MatchingActionDOB> matchingActions;
}
